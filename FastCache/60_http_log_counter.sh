#!/bin/bash

# Metric value definition:
# 0: normal
# -1: error
# >0: abnormal

# Get Http Status Count.
#   Return an integer to zabbix.
function http_status_count() {

    # Arg Parse/Create Keyword:
    if [ "x${1}" = "x1XX" ] | [ "x${1}" = "x1xx" ] ; then
        local keyword="1001"
    elif [ "x${1}" = "x2XX" ] | [ "x${1}" = "x2xx" ] ; then
        local keyword="1002"
    elif [ "x${1}" = "x3XX" ] | [ "x${1}" = "x3xx" ] ; then
        local keyword="1003"
    elif [ "x${1}" = "x4XX" ] | [ "x${1}" = "x4xx" ] ; then
        local keyword="1004"
    elif [ "x${1}" = "x5XX" ] | [ "x${1}" = "x5xx" ] ; then
        local keyword="1005"
    else
        echo "-1"
        return 1
    fi

    # Ignore domain list:
    if [ "x${2}" != "x" ] ; then
        local ignore_domain=()
        while [ "x${2}" != "x" ] ; do
            ignore_domain=("${ignore_domain[@]}" "${2}")
            shift
        done
    fi

    # Time:
    local today=$(date +%Y%m%d)
    local tomorrow=$(date -d tomorrow +%Y%m%d%H%M)
    local hour=$(date +%H)

    local min_p1=$(date +%M | sed -e "s/\([0-9]\)[0-9]/\1/g")
    local min_p2=$(date +%M | sed -e "s/[0-9]\([0-9]\)/\1/g")
    if [ ${min_p2} -lt 5 ] ; then
        local min="${min_p1}0"
    else
        local min="${min_p1}5"
    fi

    # Logdir/LogFile:
    local logdir_squid="/cache/logs/data"
    local logdir_fastcache="/cache/logs/fcache_data"
    if [ -d "${logdir_fastcache}" ] ; then
        local logdir="${logdir_fastcache}"
        local log="${today}${hour}${min}.tmp"
    elif [ -d "${logdir_squid}" ] ; then
        local ts=$(echo "${today}${hour}${min}" | sed "s/\([0-9]\{4\}\)\([0-9]\{2\}\)\([0-9]\{2\}\)\([0-9]\{2\}\)\([0-9]\{2\}\)/\1-\2-\3 \4:\5/g")
        local logdir="${logdir_squid}"
        # Squid need delay 5 mins:
        local log="$(($(date +%s -d "${ts}") - 300)).tmp"
    else
        echo 0
        return 1
    fi

    if [ -f "${logdir}/${log}" ] ; then
        local logfile="${logdir}/${log}"
    elif [ -f "${logdir}/${today}/${log}" ] ; then
        local logfile="${logdir}/${today}/${log}"
    elif [ -f "${logdir}/${tomorrow}/${log}" ] ; then
        local logfile="${logdir}/${tomorrow}/${log}"
    else
        echo 0
        return 1
    fi

    # Ignore Domain Set:
    if [ ${#ignore_domain[@]} -ne 0 ] ; then
        local ignore_domain_regex=$(echo ${ignore_domain[@]} | sed -e "s_ _\\\|_g")
    else
        local ignore_domain_regex="^$"
    fi

    local count=$(grep ".*\"${keyword} [0-9]* [0-9]*\".*$" "${logfile}" | \
                    grep -v "${ignore_domain_regex}" | \
                    sed -e "s/.*\"${keyword} \([0-9]*\) [0-9]*\".*$/\1/g" | \
                    awk 'BEGIN {sum = 0} {sum += $1} END {print sum}')

    [ $count -gt 1433476929 ] && echo 0 || echo $count
    return 0

}

function alarm_5xx {
    local http_2xx=$(http_status_count 2xx)
    local http_3xx=$(http_status_count 3xx)
    local http_4xx=$(http_status_count 4xx "www.d-fone.com" "vod.cntv.dns.cictv.tv" "dl.uu.cc")
    local http_5xx=$(http_status_count 5xx)

    # > 0.4 means more than 40%. +1 for prevent divide by zero.
    local percent_5xx=$(echo "scale=2; ${http_5xx} / (${http_2xx} + 1) * 100" | bc)

    # A and (B or C)
    if [ "$http_5xx" -gt "300" ] && ( [ "$http_2xx" -eq "0" ] || [ "${percent_5xx}" -gt "40" ] ); then
        echo Error 
        return 1
    else
        echo OK
        return 0
    fi
}

msg=$(alarm_5xx)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"http.log.error.5xx\",\
  \"value\"      : $retval,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
