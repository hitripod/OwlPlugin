#!/bin/bash

# Metric value definition:
# 0: OK
# 1: Error

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH


function heka_enabled {
    grep -q "^server.access_log.*hekad.*$" /usr/local/fastcache/etc/fastcache.conf > /dev/null 2>&1
    return $?
}


function heka_alived {
    pgrep hekad > /dev/null 2>&1
    return $?
}

function heka_trans_check {

    if (sudo test ! -f /cache/logs/hekad/logstreamer/LogstreamerInput ); then
        echo Error
        return 1
    fi

    local heka_log_time=`sudo grep -oP '[0-9]{10}(?=\.log)' /cache/logs/hekad/logstreamer/LogstreamerInput`
    local local_time=`date '+%Y%m%d%H'`
    local local_minute=`date '+%M'`

    if [ $heka_log_time != $local_time ] && [ $local_minute -gt 2 ]; then
        return 1
    else
        return 0
    fi
}

function heka_running {
    if heka_enabled && ( ! heka_alived ); then
        echo Error
        return 1
    else
        echo OK
        return 0
    fi
}

function heka_trans_status {
    if heka_enabled && ( ! heka_trans_check ); then
        echo Error
        return 1
    else
        echo OK
        return 0
    fi
}

# Call function
msg1=$(heka_running)
retval1=$?
msg2=$(heka_trans_status)
retval2=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.heka.alived\",\
    \"value\"      : $retval1,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60},\
    {\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"check.heka.status\",\
    \"value\"      : $retval2,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"

