#!/bin/bash

# Metric value definition:
# 0: OK
# 1: Crond is not running

# Check crond status.
#   Return error str if crond not running, otherwise return str "OK".
function crond_stat() {
    local PID=/var/run/crond.pid
    [ -f "$PID" ] && grep -q crond /proc/`cat $PID`/cmdline 2>/dev/null
    if [ $? -eq 0 ]; then
        echo OK
        return 0
    else
        echo "Crond not running.."
        return 1
    fi
}

msg=$(crond_stat)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"sys.crond\",\
  \"value\"      : $retval,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
