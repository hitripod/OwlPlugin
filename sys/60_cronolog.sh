#!/bin/bash

# Metric value definition:
# 0: OK
# 1: 流量回报cronolog未运行
# 2: %Y%m%d%H_error.log未运行
# 3: 访问日志%Y%m%d%H%M.log未运行

function cronolog_stat() {
    # pre check
    test -f /etc/init.d/fastcache && /etc/init.d/fastcache status &>/dev/null
    if [ $? -ne 0 ]; then
        echo OK
        return 0
    fi

    # check
    if grep -q '^stat_flux_log.*fcache_data/%Y%m%d%H%M.tmp' /home/fastcache_conf/fastcache_private; then
        if ! ps -ef |grep -q '[c]ronolog.*/cache/logs/fcache_data'; then
            echo "流量回报cronolog未运行"
            return 1
        fi
    fi
    if ! ps -ef |grep -q '[c]ronolog.*/cache/logs/%Y%m%d%H_error.log'; then
        echo "%Y%m%d%H_error.log未运行"
        return 2
    fi
    if grep -q '^server.access_log.*/cache/logs/%Y%m%d%H_access.log' /home/fastcache_conf/fastcache_private; then
        if ! ps -ef |grep -q '[c]ronolog.*/cache/logs/%Y%m%d%H_access.log'; then
            echo "访问日志%Y%m%d%H%M.log未运行"
            return 3
        fi
    else
        if ! ps -ef |grep -q '[c]ronolog.*/cache/logs/cronolog/%Y%m%d%H%M/%Y%m%d%H%M.log'; then
            echo "访问日志%Y%m%d%H%M.log未运行"
            return 3
        fi
    fi

    # default
    echo OK && return 0
}

msg=$(cronolog_stat)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"sys.cronolog\",\
  \"value\"      : $retval,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
