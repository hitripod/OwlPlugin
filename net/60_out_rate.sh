#!/bin/bash

# Metric value definition:
# network interface out rate

function net_out_rate()
{
    [ $# -ne 1 ] && echo 0.00 && return 1
    local link_speed=$(sudo /sbin/ethtool $1 2> /dev/null | grep -i speed | awk '{print $2}' | sed -e "s/\([0-9]*\)Mb\/s/\1/g")
    local out_speed=$(sar -n DEV 2 1 | awk '/'$1'/{printf "%d",$7*8/1000;exit}')
    if ( echo "$link_speed" | grep -q '^[0-9]+$' ) ; then
        awk 'BEGIN{printf "%0.2f\n", '$out_speed' / '$link_speed'}'
    else
        echo 0.00
    fi
    return 0
}

# Call function
msgEth0=$(net_out_rate eth0)
msgEth1=$(net_out_rate eth1)
msgEm1=$(net_out_rate em1)
msgBond0=$(net_out_rate bond0)
retval=$?
date=`date +%s`
host=$HOSTNAME

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"interface=eth0\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"net.out_rate\",\
    \"value\"      : $msgEth0,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60\
    },{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"interface=eth1\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"net.out_rate\",\
    \"value\"      : $msgEth1,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60\
    },{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"interface=em1\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"net.out_rate\",\
    \"value\"      : $msgEm1,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60\
    },{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"interface=bond0\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"net.out_rate\",\
    \"value\"      : $msgBond0,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"
