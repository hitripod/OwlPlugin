#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH


function chk_mistiming() {
    /bin/sleep "$[RANDOM%600]"
    
    local rdate_time=`/usr/bin/rdate  -p ntp1.cachecn.net  |head -1 | awk -F "]" '{print $2}'`
    if [ -z "$rdate_time" ] ; then
        local rdate_time=`/usr/bin/rdate  -p ntp1.cachecn.net  |head -1 | awk -F "]" '{print $2}'`
        [ -z "$rdate_time" ] && return
    fi
    local local_time=`date '+%s'`
    local server_time=`/bin/date '+%s' -d "$rdate_time"`
    local mistiming=`expr $local_time - $server_time`

    echo ${mistiming#-}
}


# Call function
mistiming=$(chk_mistiming)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"chk.mistiming\",\
    \"value\"      : $mistiming,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"

