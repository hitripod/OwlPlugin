#!/bin/bash

# Metric value definition:
# 0: OK
# 1: Error

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function dns_check {
    local dns_host1=${1}
    local dns_host2=${2}

    dns_ip=`host -W 1 $dns_host1 |grep -oP '(\d{1,3}\.){3}\d{1,3}' | head -1`

    if [ -z $dns_ip ]; then
        # One more try for live streaming media.

        dns_ip2=`host -W 1 $dns_host2 |grep -oP '(\d{1,3}\.){3}\d{1,3}' | head -1`
        if [ -z $dns_ip2 ]; then
            # ERROR
            echo 1
            exit 1
        else
            # OK
            echo 0
            exit 0
        fi

    else
        # OK
        echo 0
        exit 0
    fi
}


# Call function
msg=$(dns_check "www.taobao.com" "www.fastweb.com.cn")
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"chk.dns_check\",\
    \"value\"      : $msg,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"
