#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

endpoint=`hostname -s`
value=`curl --connect-timeout 10 -m 10 -o /dev/null -s -w %{time_total} http://127.0.0.1:80`
timestamp=`date +%s`
value=$(echo "$value*1000"|bc)
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"http.response.time\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60}]
