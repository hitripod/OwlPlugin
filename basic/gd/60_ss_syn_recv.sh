#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

endpoint=`hostname -s`
value=`ss -o state syn-recv |wc -l`
timestamp=`date +%s`
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"ss.syn.recv\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60}]