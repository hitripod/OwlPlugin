#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

endpoint=`hostname -s`
value=`ss -o state close-wait | wc -l`
timestamp=`date +%s`
echo -n [{\"endpoint\": \"$endpoint\", \"tags\": \"\", \"timestamp\": $timestamp, \"metric\": \"ss.close.wait\", \"value\": $value, \"counterType\": \"GAUGE\", \"step\": 60}]