#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH


function pkg_installed() {
    local pkg_name=$1
    if (rpm -q $pkg_name); then
        return 0
    else
        return 1
    fi
}

function service_running() {
    local service_name=$1
    if (service ${service_name} status); then
        return 0
    else
        return 1
    fi
}

function main(){
    local service_name=$1
    # 已经安装agent，但是agent没有启动，则告警
    if (pkg_installed $service_name) && !(service_running $service_name); then
        return 1
    else
        return 0
    fi
}


# Call function

pkg_name="zabbix-agent"
msg=$(main $pkg_name)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"zabbix-agent.alive\",\
    \"value\"      : $retval,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 300}]"
