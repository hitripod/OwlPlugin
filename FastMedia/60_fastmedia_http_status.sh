#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
export PATH

function fastmedia_http_status() {

    /usr/bin/curl -s -o /dev/null -w '%{http_code} %{time_connect} %{time_total} %{speed_download}\n' http://chk.fastweb.com.cn/test.flv?speed=fw-H86J1RH9qLLx -x 127.0.0.1:8889  -m 10

}




# Call function
msg=$(fastmedia_http_status)
retval=$?

msgCode=$(echo $msg | awk '{print $1}')
msgTimeConnect=$(echo $msg | awk '{print $2}')
msgTimeTotal=$(echo $msg | awk '{print $3}')

speed_download=$(echo $msg | awk '{print $4}')
# Convert bytes to bits
msgSpeed=$(echo "$speed_download" '*' "8" | bc)

date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"fm.http.code\",\
    \"value\"      : $msgCode,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60\
    },{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"fm.http.time.connect\",\
    \"value\"      : $msgTimeConnect,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60\
    },{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"fm.http.time.total\",\
    \"value\"      : $msgTimeTotal,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60\
    },{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"fm.http.speed\",\
    \"value\"      : $msgSpeed,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60\
    }]"
