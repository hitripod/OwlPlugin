#!/bin/bash

# Metric value definition:
# 0: OK
# 1: Recovering
# 2: Degraded

# Software raid status check.
#   Return str "OK" if no "degraded", otherwise return error str.
function md_stat() {

    for device in $(ls /dev/md* 2> /dev/null) ; do

        if [ -b "${device}" ] ; then

            local md_stat=$(sudo /sbin/mdadm -D ${device} | grep "State :" 2> /dev/null)

            echo ${md_stat} | grep -q -i "recovering"
            if [ $? -eq 0 ] ; then
                device=$(echo ${device} | awk -F '/' '{print $3}' | tr "[a-z]" "[A-Z]")
                echo "${device} Recovering"
                return 1
            fi

            echo ${md_stat} | grep -q -i "degraded"
            if [ $? -eq 0 ] ; then
                device=$(echo ${device} | awk -F '/' '{print $3}' | tr "[a-z]" "[A-Z]")
                echo "${device} Degraded"
                return 2
            fi

        else
            continue
        fi

    done

    echo "OK"
    return 0
}

# Call function
msg=$(md_stat)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Parse message
if [ 0 != "$retval" ] ; then
    tag=$(echo $msg | awk '{print "device=" $1}')
fi

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"dev.software_raid\",\
    \"value\"      : $retval,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"
