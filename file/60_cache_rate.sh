#!/bin/bash

# Metric value definition:
# #rate

HOST_IP=$(/sbin/ip route show | awk 'BEGIN {ip = ""} /kernel/{ for(i = 1; i <= NF; i++) { if(($i == "src") && ($(i + 1) !~ /^(192\.168\.|172\.16|10\.)/)) { ip = $(i + 1) } } } END {print ip}')

# Get Cache Hit Rate of Squid/Fastcache.
#   Return a float to zabbix.
#       $1 - Type of Hit Rate. support:
#           ReqHit_60 / ReqHit_5 / ByteHit_60 / ByteHit_5
function cache_rate() {

    # Args Check
    if [ -n "${hit_type}" ] || [ -z "${HOST_IP}" ]; then
        echo 0
        return 1
    fi

    local hit_type="${1}"
    local ip=${HOST_IP}

    # Detect Cache Software Type:
    if [ -f /usr/local/fastcache/etc/fastcache.conf ]; then
        local cache_type="fastcache"
    elif [ -d /usr/local/squid/ ]; then
        local cache_type="squid"
    else
        echo 0 && return 2
    fi

    # Fastcache
    if [ "x${cache_type}" = "xfastcache" ]; then

        local fc_stat_runtime="/usr/local/fastcache/logs/fcache_stat_runtime.log"

        # finfo
        if [ ! -r "${fc_stat_runtime}" ] && [ ! -w "${fc_stat_runtime}" ] ; then
            echo 0 && return 2
        fi

        #local hit=$(/usr/local/fastcache/bin/finfo -H -o | grep -a "|Hit" | sed "s/.*|Hit *|\([0-9]*\).*/\1/" | tail -n1)
        #local miss=$(/usr/local/fastcache/bin/finfo -H -o | grep -a "|Miss" | sed "s/.*|Miss *|\([0-9]*\).*/\1/" | tail -n1)

        #if [ -n "${hit}" ] && [ -n "${miss}" ] && [ "${hit}" -eq 0 ] && [ "${miss}" -eq 0 ] ; then
        #    local result=0
        #else
        #    local result=$(echo "scale=4;${hit}/(${hit}+${miss})*100" | bc | sed 's/00$//')
        #    [ -z "${result}" ] && result=0
        #fi

        case ${hit_type} in
            ReqHit_60|ReqHit_5)
                local req_hit=$( /usr/local/fastcache/bin/finfo -H -o |grep -Po 'Cache Hit\s+\|[\d.]+' |sed -r 's/[^0-9.]+\|//' | tail -n1)
                [ -n "${req_hit}" ] && echo ${req_hit} || echo 0
            ;;
            ByteHit_60|ByteHit_5)
                local byte_hit=$( /usr/local/fastcache/bin/finfo -H -o |grep -Po 'Size Hit\s+\|[\d.]+'  |sed -r 's/[^0-9.]+\|//' | tail -n1)
                [ -n "${byte_hit}" ] && echo ${byte_hit} || echo 0
            ;;
            *)          echo 0 && return 3 ;;
        esac

    fi

    # Squid
    if [ "x${cache_type}" = "xsquid" ]; then

        case ${hit_type} in
            ReqHit_60)  local result=$(/usr/local/squid/bin/squidclient -l127.0.0.1 -h ${ip} -p80 mgr:info | awk '/Request Hit Ratios:/{gsub(/[,%]/,"",$5); print $5}') ;;
            ReqHit_5)   local result=$(/usr/local/squid/bin/squidclient -l127.0.0.1 -h ${ip} -p80 mgr:info | awk '/Request Hit Ratios:/{gsub(/[,%]/,""); print $NF}') ;;
            ByteHit_60) local result=$(/usr/local/squid/bin/squidclient -l127.0.0.1 -h ${ip} -p80 mgr:info | awk '/Byte Hit Ratios:/{gsub(/[,%]/,"",$5); print $5}') ;;
            ByteHit_5)  local result=$(/usr/local/squid/bin/squidclient -l127.0.0.1 -h ${ip} -p80 mgr:info | awk '/Byte Hit Ratios:/{gsub(/[,%]/,""); print $NF}') ;;
            *)          echo "0" && return 3 ;;
        esac

        if [ -z "${result}" ] ; then
            echo 0
        else
            echo ${result}
        fi

    fi
}

msgReqHit60=$(cache_rate ReqHit_60)
msgReqHit5=$(cache_rate ReqHit_5)
msgByteHit60=$(cache_rate ByteHit_60)
msgByteHit5=$(cache_rate ByteHit_5)
retval=$?
date=`date +%s`
host=$HOSTNAME

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"type=ReqHit_60\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"file.cache.rate\",\
  \"value\"      : $msgReqHit60,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60\
  },{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"type=ReqHit_5\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"file.cache.rate\",\
  \"value\"      : $msgReqHit5,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60\
  },{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"type=ByteHit_60\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"file.cache.rate\",\
  \"value\"      : $msgByteHit60,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60\
  },{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"type=ByteHit_5\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"file.cache.rate\",\
  \"value\"      : $msgByteHit5,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
