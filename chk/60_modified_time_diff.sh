#!/bin/bash

# Metric value definition:
# seconds

# Compare value of Http header "Last-Modified" between customer's source and our cache.
#   Return time difference (in second) integer to zabbix.
#       $1 - Source Domain name or IP (example: 117.135.138.140)
#       $2 - uri (example: data/xml/weather/200/33.xml)
#       $3 - CDN Domain name (example: cdn.moji001.com)
function modified_time_diff() {

    # Args Check:
    if [ $# -lt 3 ] ; then
        echo "Err"
        return 1
    fi

    local source_host="${1}"
    local request_uri="${2}"
    local cdn_host="${3}"

    # Source/Our Response header:
    local source_response_header="/tmp/${PID}_ZBX_CHK_FW_MODIFIED_TIME_DIFF_SRH"
    /usr/bin/curl -s -D "${source_response_header}" -o /dev/null -f -m 2 "http://${source_host}/${request_uri}" 2> /dev/null
    local cdn_response_header="/tmp/${PID}_ZBX_CHK_FW_MODIFIED_TIME_DIFF_CRH"
    /usr/bin/curl -s -D "${cdn_response_header}" -o /dev/null -f -m 2 -x 127.0.0.1:80 "http://${cdn_host}/${request_uri}" 2> /dev/null

    # Make sure Response not Null
    if [ -f "${source_response_header}" ] && [ -f "${cdn_response_header}" ] ; then

        # Source/Our Last-Modified header:
        local source_modified_time_str="$(grep "Last-Modified" "${source_response_header}" | awk '{print $2,$3,$4,$5,$6,$7}')"
        local cdn_modified_time_str="$(grep "Last-Modified" "${cdn_response_header}" | awk '{print $2,$3,$4,$5,$6,$7}')"

        # Make sure Last-Modified header not Null
        if [ -n "${source_modified_time_str}" ] && [ -n "${cdn_modified_time_str}" ] ; then

            local source_modified_time=$(date +%s -d "${source_modified_time_str}")
            local cdn_modified_time=$(date +%s -d "${cdn_modified_time_str}")

            local time_diff=$((source_modified_time - cdn_modified_time))

            if [ ${time_diff} -ge 0 ] ; then
                echo ${time_diff}
            else
                echo 0
            fi

        # Last-Modified header is Null
        else
            echo 0
        fi

    # Response is Null
    else
        echo 0
    fi

    # Cleanup:
    [ -f "${source_response_header}" ] && rm -f "${source_response_header}"
    [ -f "${cdn_response_header}" ] && rm -f "${cdn_response_header}"

    return 0

}

msg=$(modified_time_diff purge.mon.fastweb.com.cn purge_mon.php purge.mon.fastweb.com.cn)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"chk.modified_time_diff\",\
  \"value\"      : $msg,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
