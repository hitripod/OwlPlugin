#!/bin/bash

# Metric value definition:
# -3: ConnectionRefused
# -2: RequestTimeOut
# -1: error
# >=0: time or code

# Check URL access via a proxy.
#   Return float (tim mode) / string (stat mode) to zabbix.
#       $1 - Check type: tim (how long for GET) or stat (Status of GET)
#       $2 - URL to access.
#       $3 - Proxy IP (Default: Host Interface IP)
function access() {

    local default_ip="${HOST_IP}"

    # Args Check:
    if [ $# -lt 2 ] ; then
        echo -1
        return 1
    fi

    # $1 - Check type:
    if [ "x${1}" != "xtime" ] && [ "x${1}" != "xstat" ] ; then
        echo -1
        return 1
    else
        local chk_option="${1}"
    fi

    # $2 - url to check:
    local url="${2}"

    # $3 - use default if not given:
    if [ -n "${3}" ] ; then
        local proxy_ip="${3}"
    else
        local proxy_ip="${default_ip}"
    fi

    case "${chk_option}" in
        time)
        local rv=$(/usr/bin/curl -o /dev/null -s -m 3 -x ${proxy_ip}:80 -w "%{time_total}" $url 2> /dev/null)
        echo ${rv}
        ;;
        stat)
        # rv=$(/usr/bin/curl -o /dev/null -s -m 3 -x ${proxy_ip}:80 -w "%{http_code}" $url 2> /dev/null)
        # [ "x${rv}" = "x000" ] && rv="RequestTimeOut" || rv="Http Status ${rv}"
        local rv=$(/usr/bin/wget --max-redirect=0 --timeout=3 --tries=1 -S -e http-proxy="${proxy_ip}" --user-agent="zabbix wget" -O /dev/null "${url}" 2>&1 | grep "\(failed: Connection refused\|HTTP.1\..\)" | awk '{if ($2 ~ /^[0-9]+/) {print $2} else {gsub(/ refused/,"Refused", $0); gsub(/\./,"", $0); print $NF}}')
        if [ "x${rv}" == "x" ] ; then
            rv=-2
        elif [ "x${rv}" == "xConnectionRefused" ] ; then
            rv=-3
        fi
        echo ${rv}
        ;;
    esac

}

# Call function
msgRespTime=$(access time "http://purge.mon.fastweb.com.cn/100k.jpg" 127.0.0.1)
msgRespCode=$(access stat "http://purge.mon.fastweb.com.cn/100k.jpg" 127.0.0.1)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"chk.access.time\",\
    \"value\"      : $msgRespTime,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60\
    },{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"chk.access.code\",\
    \"value\"      : $msgRespCode,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"
