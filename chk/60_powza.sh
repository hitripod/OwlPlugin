#!/bin/bash

# Metric value definition:
# 0: OK
# 1: No listening port
# 2: Miss symbolic link,directory or file 
# 3: Miss mounted path
# 4: Usage of mounted path is higher than 80%
# 5: Not mounted
# 6: /data/proclog/log/pzs/ does not exist
# 7: file does not exist
# 8: Restart times are more than 5

function powza_stat() {
        #Check LISTEN Ports :80(pz_nx),8000(pzs),7000(pzs),1935(pzs)
        for port in {80,8000,7000,1935};do
                echo "nc_port_check" |nc -w1 -n -v 127.0.0.1 $port || { echo "无监听$port端口"; return 1; }
        done
        #check symbolic link,directory or file exit
        for link in {"/cache","/diskcache","/play","/file","/memcache"}; do
                 [ -L "$link" ]  || { echo "软连接$link不存在"; return 2; }
        done
        #Check Mounted paths
        local usage_data=$(df /data/ | awk 'NR==2{sub("%","");print $5}')
        local usage_logs=$(df /logs/ | awk 'NR==2{sub("%","");print $5}')
        for mount_path in /data /logs; do
            [ ! -d "$mount_path" ] && echo "目录$mount_path不存在" && return 3
            if (df | awk 'NR>1{print $6}' | grep -q -x "$mount_path") ; then
                [ "$usage_data" -gt 80 ] && echo "目录$mount_path使用率超过80%" && return 4
            else
                echo "目录$mount_path没有mount"
                return 5
            fi
        done
        #Check path
        [ -d /data/proclog/log/pzs/ ] || { echo "目录/data/proclog/log/pzs/不存在"; return 6; }
        #Check file
        for filename in /file/check000.flv /play/crossdomain.xml /usr/local/pzs/license.dat; do
            [ ! -e "$filename" ] && echo "文件$filename不存在" && return 7
        done
        #Check restart times
        local num_restart=$(grep -wc 'restart' "/usr/local/pzs/admin_log.txt")
        if [ "$num_restart" -gt 5 ] ; then
            echo "重启次数超过五次"
            return 8
        fi
        echo "OK"
        return 0
}

# Call function
msg=$(powza_stat)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"chk.powza\",\
    \"value\"      : $retval,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"
