#!/bin/bash

# Metric value definition:
# 0: false
# 1: true
# 2: error

# Check DNS Query.
#   Return a bool to zabbix.
#       $1 - ServerIP (Default: Host Interface IP)
#       $2 - Query FQDN (Default: zabbix.fastweb.com.cn)
function dns_query() {

    local default_ip="${HOST_IP}"
    local default_fqdn="zabbix.fastweb.com.cn"

    # Args Check:
    # Use default ip and default fqdn if ip/fqdn both not given.
    if [ -z "${1}" ] && [ -z "${2}" ] ; then
        local nameserver="${default_ip}"
        local query_fqdn="${default_fqdn}"
    # Use default fqdn if fqdn not given.
    elif [ -n "${1}" ] && [ -z "${2}" ] ; then
        local nameserver="${1}"
        local query_fqdn="${default_fqdn}"
    # ip/fqdn both given.
    elif [ -n "${1}" ] && [ -n "${2}" ] ; then
        local nameserver="${1}"
        local query_fqdn="${2}"
    # Just in case.
    else
        echo 2
    fi

    dig +time=2 +tries=1 @${nameserver} ${query_fqdn} > /dev/null 2>&1
    if [ $? -eq 0 ] ; then
        echo 0
    else
        echo 1
    fi

    return 0

}

# Call function
msg=$(dns_query 127.0.0.1 www.fastweb.com.cn)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"chk.dns_query\",\
    \"value\"      : $msg,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 60}]"
