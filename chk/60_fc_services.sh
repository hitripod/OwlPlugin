#!/bin/bash

# Metric value definition:
# 0: OK
# 1: fwcm not running
# 2: prelog not running
# 3: fwlog not running

function fc_services_stat()
{
    ( /FastwebApp/cm/bin/cm ping |grep -q pong ) || { echo fwcm not running; return 1; }
    sudo /sbin/service prelog status &>/dev/null || { echo prelog not running; return 2; }
    sudo /sbin/service fwlog status &>/dev/null || { echo fwlog not running; return 3; }

    echo OK
    return 0
}

msg=$(fc_services_stat)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"chk.fc_services\",\
  \"value\"      : $retval,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
