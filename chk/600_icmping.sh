#!/bin/bash

# Metric value definition:
# -1: monitor type not given
# -2:
# -3:
# -4:

# Check ICMP Ping RTT avg Time.
#   Return float to zbbbix.
#       $1 - monitor type (rtt/loss).
#       $2 - packages conut (ping -c $1).
#       $3 - IP/Hostname (ping $2).
function icmping() {

    local default_pinghost="127.0.0.1"
    local default_pingcount=5
    local monitor_type="${1}"

    # Args Check:
    # Monitor type not given.
    if [ "x${monitor_type}" != "xrtt" ] && [ "x${monitor_type}" != "xloss" ] ; then
        echo "-1"
        return 1
    # ping count and ping host both not given.
    elif [ -z "${2}" ] && [ -z "${3}" ] ; then
        local count=${default_pingcount}
        local pinghost="${default_pinghost}"
    # only ping host not given.
    elif [ -n "${2}" ] && [ -z "${3}" ] ; then
        local count=${2}
        local pinghost="${default_pinghost}"
    # ping count and ping host both given.
    elif [ -n "${2}" ] && [ -n "${3}" ] ; then
        local count=${2}
        local pinghost=${3}
    # Just in case.
    else
        echo "-2"
        return 1
    fi

    if [ ${count} -le 0 ] ; then
        echo "-3"
        return 1
    fi

    if [ "x${monitor_type}" = "xrtt" ] ; then
        local rtt=$(ping -w 2 -i 0.2 -c ${count} ${pinghost} 2> /dev/null | grep "^rtt")
        if [ -z "${rtt}" ] ; then
            echo 0 && return 1
        else
            echo ${rtt} | awk '{gsub(/\//," ",$0); print $8}'
        fi
    elif [ "x${monitor_type}" = "xloss" ] ; then
        local loss=$(ping -w 2 -i 0.2 -c ${count} ${pinghost} 2> /dev/null | grep -i "packet loss" | awk '{printf "%.2f", $6}')
        if [ -z "${loss}" ] ; then
            echo 100
            return 1
        else
            echo ${loss}
            return 0
        fi
    # This should never happen..
    else
        echo "-4"
        return 1
    fi

}

# Call function
msgLoss=$(icmping loss 5 www.baidu.com)
msgRtt=$(icmping rtt 5 www.baidu.com)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

# Send JSON message
echo "[{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"chk.icmping\",\
    \"value\"      : $msgLoss,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 600\
    },{\
    \"endpoint\"   : \"$host\",\
    \"tags\"       : \"$tag\",\
    \"timestamp\"  : $date,\
    \"metric\"     : \"chk.icmping\",\
    \"value\"      : $msgRtt,\
    \"counterType\": \"GAUGE\",\
    \"step\"       : 600}]"
