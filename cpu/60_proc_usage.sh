#!/bin/bash

# Metric value definition:
# -1: Error
# >= 0: Usage of process

# Get CPU usage of some process.
#   Return float of usage to zabbix.
#       $1 - Process Name
function cpu_proc() {

    # Args Check and Set process name.
    if [ "x${1}" = "x" ] ; then
        echo -1
        return 1
    else
        local proc="${1}"
    fi

    # The keyword for "ps -ef" way.
    if [ "x${proc}" = "xsquid" ] ; then
        local keyword="(squid)"
    elif [ "x${proc}" = "xfastcache" ] ; then
        local keyword="fastcache:"
    elif [ "x${proc}" = "xnginx" ] ; then
        local keyword="nginx:"
    else
        echo -1
        return 1
    fi

    #ps -ef | grep "${keyword}" | grep -v grep | awk 'BEGIN { max = 0 } { if ($4 > max) { max = $4 } } END {print max}'
    top -b -n 2 -d 1 | awk '$NF=="'${proc}'"{ p[$1] += $9 } END { for(i in p) { if(maxsum < p[i]) { maxsum = p[i] }; maxsum == 0 ? maxsum = 3 : maxsum }; printf "%d\n", maxsum/3 }' 2> /dev/null || echo 0

    return 0

}

msgSquid=$(cpu_proc squid)
msgFastcache=$(cpu_proc fastcache)
msgNginx=$(cpu_proc nginx)
retval=$?
date=`date +%s`
host=$HOSTNAME

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"proc=squid\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"cpu.usage\",\
  \"value\"      : $msgSquid,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60\
  },{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"proc=fastcache\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"cpu.usage\",\
  \"value\"      : $msgFastcache,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60\
  },{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"proc=nginx\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"cpu.usage\",\
  \"value\"      : $msgNginx,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
