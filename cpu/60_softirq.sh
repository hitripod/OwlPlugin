#!/bin/bash

# Metric value definition:
# 0: OK
# 1: soft irq
# 2: irq

function soft_irq() {
    os_release=`sed 's/.* \([0-9.]*\) .*/\1/' /etc/redhat-release`
    if ( echo $os_release |egrep -q '^6' ); then
        mpstat -P ALL |awk '$3~/^[0-9]+$/&&$NF<20{if($9>40){printf("CPU:%d %d%\n",$3,100-$NF); exit 1}}'
        retval=$?
        [ $retval -eq 0 ] && echo OK
    else
        mpstat -P ALL |awk '$3~/^[0-9]+$/&&$(NF-1)<70{if($8>0){printf("CPU:%d %d%\n",$3,100-$(NF-1)); exit 2}}'
        retval=$?
        [ $retval -eq 0 ] && echo OK
    fi
    return $retval
}

msg=$(soft_irq)
retval=$?
date=`date +%s`
host=$HOSTNAME
tag=""

echo "[{\
  \"endpoint\"   : \"$host\",\
  \"tags\"       : \"$tag\",\
  \"timestamp\"  : $date,\
  \"metric\"     : \"cpu.soft_irq\",\
  \"value\"      : $retval,\
  \"counterType\": \"GAUGE\",\
  \"step\"       : 60}]"
